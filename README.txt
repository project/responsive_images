Description
===========
This module integrates the Responsive Images library created by Filament Group:
https://github.com/filamentgroup/Responsive-Images

The purpose of this library is to load images in different sizes based upon
the resolution of the browser loading the page. This allows for mobile-optimized images.

This module provides a "Responsive Images" image field formatter where you can select
both a small (mobile-optimized) image style as well as a large (for normal screens) image style.
The image style configuration options are provided by Drupal core.

Installation
=============
1/ Download the Responsive Images library (https://github.com/filamentgroup/Responsive-Images) to your sites/all/libraries folder
   Make sure to put it in a folder named "responsive_images".
   The folder structure should be like sites/[all or sitename]/libraries/responsive_images/rwd-images/
   Optionally you can use the Libraries API module (http://drupal.org/project/libraries) to install the library in another location and have it auto detected
   Note: You don't need to follow installation instructions provided by the Responsive Images library. This will be handled in step 3.
2/ Enable the Responsive Images module
3/ Follow the configuration instructions provided at admin/config/media/responsive_images/instructions to update your .htaccess file
4/ Set the formatter for your images to "Responsive Image" (via 'Manage Displays' on your content type, field settings in Views, ...)
   Click the configure button to select the small & large image styles to be used when rendering the image

NOTE:
  This currently has only been tested for the 'master' branch of the Responsive Images library.
  The 'cookie-driven' branch is untested at the moment

Author
======
Sven Decabooter (http://drupal.org/user/35369)
The author can be contacted for paid customizations of this module as well as Drupal consulting and development.
